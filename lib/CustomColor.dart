import 'package:flutter/material.dart';

class CustomColor {
  static final Color onBoardScreen1Background = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color onBoardScreen1Tab = Colors.white;
  static final Color onBoardScreen1PageIndicator = Color.fromRGBO(255, 187, 0, 1.0);
  static final Color onBoardScreen1NextButton = Color.fromRGBO(255, 187, 0, 1.0);
  static final Color onBoardScreen1Text = Color.fromRGBO(42, 55, 71, 1.0);

  static final Color onBoardScreen2Background = Colors.white;
  static final Color onBoardScreen2Tab = Color.fromRGBO(242, 72, 99, 1.0);
  static final Color onBoardScreen2PageIndicator = Colors.white;
  static final Color onBoardScreen2NextButton = Colors.white;
  static final Color onBoardScreen2Text = Colors.white;

  static final Color onBoardScreen3Background = Colors.white;
  static final Color onBoardScreen3Tab = Color.fromRGBO(0, 173, 152, 1.0);
  static final Color onBoardScreen3PageIndicator = Colors.white;
  static final Color onBoardScreen3NextButton = Colors.white;
  static final Color onBoardScreen3Text = Colors.white;

  static final Color onBoardScreen4Background = Colors.white;
  static final Color onBoardScreen4Tab = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color onBoardScreen4PageIndicator = Colors.white;
  static final Color onBoardScreen4NextButton = Colors.white;
  static final Color onBoardScreen4Text = Colors.white;

  static final Color signScreen5Background = Color.fromRGBO(242, 72, 99, 1.0);
  static final Color signScreen5Tab = Colors.white;
  static final Color signScreen5NextButton = Color.fromRGBO(242, 72, 99, 1.0);
  static final Color signScreen5Text = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color signScreen5Card = Color.fromRGBO(236, 236, 237, 1.0);

  static final Color signScreen6Background = Color.fromRGBO(0, 173, 152, 1.0);
  static final Color signScreen6Tab = Colors.white;
  static final Color signScreen6NextButton = Color.fromRGBO(0, 173, 152, 1.0);
  static final Color signScreen6Text = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color signScreen6Card = Color.fromRGBO(236, 236, 237, 1.0);

  static final Color signScreen7Background = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color signScreen7Tab = Colors.white;
  static final Color signScreen7NextButton = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color signScreen7Text = Color.fromRGBO(42, 55, 71, 1.0);
  static final Color signScreen7TickButton = Color.fromRGBO(52, 196, 126, 1.0);
  static final Color signScreen7Card = Color.fromRGBO(236, 236, 237, 1.0);

  static final Color mainSceenBackground = Color.fromRGBO(236, 236, 237, 1.0);
  static final Color mainSceenText = Color.fromRGBO(42, 55, 71, 1.0);
}