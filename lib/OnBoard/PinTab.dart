import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class PinTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PinTabState();
}

class PinTabState extends State<PinTab> {

  int pinCount = 0;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ...getPinCircle(),
              ],
            ),

            SizedBox(height: 50.0,),

            getPinKeyBoard(),
          ],
        ),
      ),
    );
  }

  getPinCircle() {
    List<Widget> pinList = [];

    for (int i = 0; i < 6; i++) {
      pinList.add(
        Container(
          height: (25 / 207) * screenWidth,
          width: (25 / 207) * screenWidth,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: CustomColor.signScreen5Card,
          ),
          child: Center(
            child: Container(
              height: (5 / 448) * screenHeight,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 1.0),
                color: (pinCount > i) ? CustomColor.signScreen5Text : CustomColor.signScreen5Card,
              ),
            ),
          ),
        )
      );
    }

    return pinList;
  }

  getPinKeyBoard() {

    double textSize = 35.0;
    FontWeight fontWeight = FontWeight.w300;
    
    double buttonSize = (35 / 207) * screenWidth;

    return Container(
      height: (160 / 448) * screenHeight,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "1",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(1),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "2",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(2),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "3",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(3),
                  ),
                ),
              ),
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "4",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(4),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "5",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(5),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "6",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(6),
                  ),
                ),
              ),
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "7",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(7),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "8",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(8),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "9",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(9),
                  ),
                ),
              ),
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Icon(
                      Icons.clear,
                      size: 30.0,
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(10),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Text(
                      "0",
                      style: TextStyle(
                        fontSize: textSize,
                        fontWeight: fontWeight
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(0),
                  ),
                ),
              ),

              Container(
                height: buttonSize,
                width: buttonSize,
                child: Center(
                  child: RawMaterialButton(
                    shape: new CircleBorder(),
                    child: Icon(
                      Icons.backspace,
                      size: 30.0,
                    ),
                    padding: const EdgeInsets.all(15.0),
                    onPressed: () => onKeyPress(11),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  
  onKeyPress(int index) {
    //Todo: add control pin
    setState(() {
      if (index == 10) {
        pinCount = 0;
      }
      else if (index == 11) {
        if (pinCount > 0) pinCount--;
      }
      else {
        if (pinCount < 6) pinCount++;
      }
    });
  }
}