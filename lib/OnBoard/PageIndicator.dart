import 'package:appbank/CustomColor.dart';
import 'package:flutter/material.dart';
import 'package:appbank/main.dart';

_indicator(bool isActive, int index) {
  return AnimatedContainer(
    height: (6 / 414) * screenWidth,
    width: (isActive) ? (14 / 414) * screenWidth : (6 / 414) * screenWidth,
    padding: EdgeInsets.all(0.0),
    decoration: BoxDecoration(
      shape: BoxShape.rectangle,
      color: (isActive) ? getIndicatorColor(index) : Color.fromRGBO(153, 153, 153, 1.0),
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    duration: Duration(milliseconds: 500),
    curve: Curves.fastOutSlowIn,
  );
}

getIndicatorColor(int index) {
  switch (index) {
    case 0:
      return CustomColor.onBoardScreen1PageIndicator;
    case 1:
      return CustomColor.onBoardScreen2PageIndicator;
    case 2:
      return CustomColor.onBoardScreen3PageIndicator;
    case 3:
      return CustomColor.onBoardScreen4PageIndicator;
  }
}

buildIndicator(int current) {
  List<Widget> list = [];
  for (int i = 0; i < 4; i++) {
    list.add(_indicator(current == i, current));
  }
  return list;
}