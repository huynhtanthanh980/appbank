import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ForgotPasswordScreenState();
}

class ForgotPasswordScreenState extends State<ForgotPasswordScreen> {

  var isPhoneChecked = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      resizeToAvoidBottomPadding: false,

      body: Container(
        height: screenHeight,
        width: screenWidth,
        color: CustomColor.signScreen7Background,
        child: Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              getSignPageWidget(),

              AnimatedContainer(
                height: (330 / 448) * screenHeight,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50.0),
                    topRight: Radius.circular(50.0),
                  ),
                  color: CustomColor.signScreen7Tab,
                ),
                padding: EdgeInsets.all((10 / 207) * screenWidth,),
                duration: Duration(milliseconds: 300),
                curve: Curves.easeOut,
                child: getSignUpTabScreen(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getSignPageWidget() {
    return AnimatedContainer(
      height: (102.6 / 448) * screenHeight,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context, MaterialPageRoute(
                builder: (context) =>
                    ForgotPasswordScreen())),
            iconSize: (10.2 / 448) * screenHeight,
            color: Colors.white,
          ),

          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              "Forgot your\nPIN?",
              style: TextStyle(
                fontSize: (20 / 448) * screenHeight,
                color: Colors.white,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
        ],
      ),
    );
  }

  getSignUpTabScreen() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: (10 / 414) * screenWidth, vertical: (10 / 448) * screenHeight,),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  child: Text(
                    "Select which contact details should we\nuse to reset your PIN",
                    style: TextStyle(
                      fontSize: (10 / 448) * screenHeight,
                    ),
                  ),
                ),

                SizedBox(height: (20 / 448) * screenHeight,),

                Container(
                  height: (57 / 448) * screenHeight,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(3.0),
                      topRight: Radius.circular(3.0),
                    ),
                    color: CustomColor.signScreen6Card,
                  ),
                  child: FlatButton(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: (140 / 207) * screenWidth,
                            padding: EdgeInsets.symmetric(horizontal: (10 / 414) * screenWidth, vertical: (12 / 448) * screenHeight,),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Via sms",
                                    style: TextStyle(
                                      fontSize: (11 / 448) * screenHeight,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),

                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Icon(
                                          Icons.phone_android,
                                          size: (11 / 448) * screenHeight,
                                        ),
                                      ),

                                      SizedBox(width: (10 / 207) * screenWidth,),

                                      Container(
                                        child: Text(
                                          "0123456789",
                                          style: TextStyle(
                                            fontSize: (11 / 448) * screenHeight,
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO(153, 153, 153, 1.0),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          (isPhoneChecked) ? getCardTick() : Container(),
                        ],
                      ),
                    ),

                    onPressed: () {
                      setState(() {
                        isPhoneChecked = true;
                      });
                    },
                  ),
                ),

                SizedBox(height: (20 / 448) * screenHeight,),

                Container(
                  height: (57 / 448) * screenHeight,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(3.0),
                      topRight: Radius.circular(3.0),
                    ),
                    color: CustomColor.signScreen6Card,
                  ),
                  child: FlatButton(
                    child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: (140 / 207) * screenWidth,
                            padding: EdgeInsets.symmetric(horizontal: (10 / 414) * screenWidth, vertical: (12 / 448) * screenHeight,),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Via email",
                                    style: TextStyle(
                                      fontSize: (11 / 448) * screenHeight,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),

                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Icon(
                                          Icons.phone_android,
                                          size: (11 / 448) * screenHeight,
                                        ),
                                      ),

                                      SizedBox(width: (10 / 207) * screenWidth,),

                                      Container(
                                        child: Flexible(
                                          child: Text(
                                            "huynhtanthanh980@gmail.com",
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: (11 / 448) * screenHeight,
                                              fontWeight: FontWeight.w400,
                                              color: Color.fromRGBO(153, 153, 153, 1.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          (isPhoneChecked) ? Container() : getCardTick(),
                        ],
                      ),
                    ),

                    onPressed: () {
                      setState(() {
                        isPhoneChecked = false;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),

          Container(
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  child: Align(
                    alignment: Alignment(1.0, 0.0),
                    child: Container(
                      height: (28 / 207) * screenWidth,
                      width: (28 / 207) * screenWidth,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: CustomColor.signScreen7NextButton,
                      ),
                      child: FlatButton(
                        child: Icon(
                          Icons.arrow_forward,
                          color: CustomColor.signScreen7Tab,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),
                ),

                SizedBox(height: (30 / 896) * screenHeight,),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getCardTick() {
    return Container(
      height: (45 / 414) * screenWidth,
      width: (45 / 414) * screenWidth,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: CustomColor.signScreen7TickButton,
      ),
      child: Icon(
        Icons.check,
        size: (35 / 896) * screenHeight,
        color: CustomColor.signScreen7Tab,
      ),
    );
  }
}