import 'package:appbank/CustomColor.dart';
import 'package:appbank/OnBoard/ForgotPasswordScreen.dart';
import 'package:appbank/OnBoard/OnboardData.dart';
import 'package:appbank/OnBoard/PageIndicator.dart';
import 'package:appbank/OnBoard/PinTab.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class OnBoardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OnBoardScreenState();
  }
}

class OnBoardScreenState extends State<OnBoardScreen> {
  int currentPage = 0;

  var pageController = new PageController();

  CustomPageView page;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        height: screenHeight,
        width: screenWidth,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            PageView.builder(
              itemCount: pageViewList.length - 1,
              controller: pageController,
              onPageChanged: (index) {
                setState(() {
                  currentPage = index;
                });
              },
              itemBuilder: (context, index) {
                page = pageViewList[index];
                return Container(
                  height: screenHeight,
                  width: screenWidth,
                  decoration: BoxDecoration(
                    image: (index != 0)
                        ? null
                        : DecorationImage(
                            image: AssetImage(
                                'assets/1x/onBoardScreen1Background.png'),
                            fit: BoxFit.cover,
                          ),
                    color: page.backgroundColor,
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: (271 / 448) * screenHeight,
                        child: Center(
                          child: page.pageWidget,
                        ),
                      ),
                      SizedBox(height: (177 / 448) * screenHeight),
                    ],
                  ),
                );
              },
            ),
            Container(
              height: double.infinity,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  (currentPage < 4) ? Container() : getSignPageWidget(),
                  AnimatedContainer(
                    height: (currentPage < 4)
                        ? (177 / 448) * screenHeight
                        : (345.4 / 448) * screenHeight,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50.0),
                        topRight: Radius.circular(50.0),
                      ),
                      color: pageViewList[currentPage].tabColor,
                    ),
                    padding: EdgeInsets.all(
                      (10 / 207) * screenWidth,
                    ),
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeOut,
                    child: getTabScreen(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getSignPageWidget() {
    return AnimatedContainer(
      height: (102.6 / 448) * screenHeight,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => pageController.animateToPage(
              0,
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeOut,
            ),
            iconSize: (10.2 / 448) * screenHeight,
            color: Colors.white,
          ),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sign in",
                    style: TextStyle(
                      fontSize: (currentPage == 4)
                          ? (20 / 448) * screenHeight
                          : (12.1 / 448) * screenHeight,
                      color: (currentPage == 4) ? Colors.white : Colors.white54,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  onPressed: () => pageController.animateToPage(
                    4,
                    duration: const Duration(milliseconds: 300),
                    curve: Curves.easeOut,
                  ),
                ),
                FlatButton(
                  child: Text(
                    "Sign up",
                    style: TextStyle(
                      fontSize: (currentPage == 5)
                          ? (20 / 448) * screenHeight
                          : (12.1 / 448) * screenHeight,
                      color: (currentPage == 5) ? Colors.white : Colors.white54,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  onPressed: () => pageController.animateToPage(
                    5,
                    duration: const Duration(milliseconds: 300),
                    curve: Curves.easeOut,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTabScreen() {
    switch (currentPage) {
      case 0:
        return getOnBoardTabScreen();
      case 1:
        return getOnBoardTabScreen();
      case 2:
        return getOnBoardTabScreen();
      case 3:
        return getOnBoardTabScreen();
      case 4:
        return getSignInTabScreen();
      case 5:
        return getSignUpTabScreen();
    }
  }

  getOnBoardTabScreen() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: (18 / 896) * screenHeight,
        ),
        Text(
          pageViewList[currentPage].title,
          style: TextStyle(
            fontSize: (19 / 448) * screenHeight,
            fontWeight: FontWeight.w300,
            color: pageViewList[currentPage].textColor,
          ),
        ),
        SizedBox(
          height: (15 / 896) * screenHeight,
        ),
        Text(
          pageViewList[currentPage].content,
          style: TextStyle(
            fontSize: (9 / 448) * screenHeight,
            fontWeight: FontWeight.w300,
            color: pageViewList[currentPage].textColor,
          ),
        ),
        SizedBox(
          height: (37 / 896) * screenHeight,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: (6 / 448) * screenHeight,
              width: (62 / 414) * screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  ...buildIndicator(currentPage),
                ],
              ),
            ),
            getNextPageArrowButton(),
          ],
        ),
        SizedBox(
          height: (30 / 896) * screenHeight,
        ),
      ],
    );
  }

  getSignInTabScreen() {
    return Flex(
      direction: Axis.vertical,
      children: <Widget>[
        Flexible(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: (10 / 448) * screenHeight,
                ),
                Container(
                  width: double.infinity,
                  height: (15 / 448) * screenHeight,
                  padding: EdgeInsets.symmetric(
                      horizontal: (10 / 414) * screenWidth),
                  child: Text(
                    pageViewList[currentPage].title,
                    style: TextStyle(
                      fontSize: (10 / 448) * screenHeight,
                    ),
                  ),
                ),
                SizedBox(
                  height: (20 / 448) * screenHeight,
                ),
                Expanded(
                  child: PinTab(),
                ),
                SizedBox(
                  height: (20 / 448) * screenHeight,
                ),
                Container(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          "Forgot Your Pin",
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20.0,
                          ),
                        ),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ForgotPasswordScreen())),
                      ),
                      getNextPageArrowButton(),
                    ],
                  ),
                ),
                SizedBox(
                  height: (30 / 896) * screenHeight,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  getSignUpTabScreen() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(
              horizontal: (10 / 414) * screenWidth,
              vertical: (10 / 448) * screenHeight,
            ),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: (15 / 448) * screenHeight,
                  child: Text(
                    pageViewList[currentPage].title,
                    style: TextStyle(
                      fontSize: (10 / 448) * screenHeight,
                    ),
                  ),
                ),
                SizedBox(
                  height: (20 / 448) * screenHeight,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: CustomColor.signScreen6Card,
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: (20 / 414) * screenWidth),
                  child: TextField(
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Full name',
                      hintStyle: TextStyle(
                        fontSize: (8 / 448) * screenHeight,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: (15 / 448) * screenHeight,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: CustomColor.signScreen6Card,
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: (20 / 414) * screenWidth),
                  child: TextField(
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Enter your email',
                      hintStyle: TextStyle(
                        fontSize: (8 / 448) * screenHeight,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: (15 / 448) * screenHeight,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: CustomColor.signScreen6Card,
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: (20 / 414) * screenWidth),
                  child: TextField(
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Enter your 6 digit PIN',
                      hintStyle: TextStyle(
                        fontSize: (8 / 448) * screenHeight,
                      ),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
                SizedBox(
                  height: (15 / 448) * screenHeight,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: CustomColor.signScreen6Card,
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: (20 / 414) * screenWidth),
                  child: TextField(
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Mobile',
                      hintStyle: TextStyle(
                        fontSize: (8 / 448) * screenHeight,
                      ),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  child: Align(
                    alignment: Alignment(1.0, 0.0),
                    child: getNextPageArrowButton(),
                  ),
                ),
                SizedBox(
                  height: (30 / 896) * screenHeight,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getNextPageArrowButton() {
    return Container(
      height: (56 / 414) * screenWidth,
      width: (56 / 414) * screenWidth,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: pageViewList[currentPage].buttonColor,
      ),
      child: FlatButton(
        child: Icon(
          Icons.arrow_forward,
          color: pageViewList[currentPage].tabColor,
        ),
        onPressed: () {
          onNextArrowButton();
        },
      ),
    );
  }

  onNextArrowButton() {
    switch (currentPage) {
      case 0:
        return nextPage();
      case 1:
        return nextPage();
      case 2:
        return nextPage();
      case 3:
        return nextPage();
      case 4:
        Navigator.of(context).pushReplacementNamed('/MainScreen');
        break;
      case 5:
        return showCustomDialog("Sign-up Function");
    }
  }

  nextPage() {
    pageController.nextPage(
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeOut,
    );
  }

  showCustomDialog(String message) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Alert",
            style: TextStyle(
              fontSize: 0.03 * screenHeight,
            ),
          ),
          content: Container(
            height: 0.025 * screenHeight,
            width: 0.4 * screenWidth,
            child: Text(
              message,
              style: TextStyle(
                fontSize: 0.02 * screenHeight,
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(
                "Done",
                style: TextStyle(
                  fontSize: 0.02 * screenHeight,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
