import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class CustomPageView {
  Widget pageWidget;
  Color backgroundColor;
  Color textColor;
  Color tabColor;
  Color buttonColor;
  String title;
  String content;

  CustomPageView(this.pageWidget, this.backgroundColor, this.textColor, this.tabColor, this.buttonColor, this.title, this.content);
}

var pageViewList = [
  CustomPageView(
    getOnBoardPageImage(1),
    CustomColor.onBoardScreen1Background,
    CustomColor.onBoardScreen1Text,
    CustomColor.onBoardScreen1Tab,
    CustomColor.onBoardScreen1NextButton,
    "Welcome to\nMyBank",
    "Lorem ipsum Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit",
  ),

  CustomPageView(
    getOnBoardPageImage(2),
    CustomColor.onBoardScreen2Background,
    CustomColor.onBoardScreen2Text,
    CustomColor.onBoardScreen2Tab,
    CustomColor.onBoardScreen2NextButton,
    "Easy wallet\nControl",
    "Lorem ipsum Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit",
  ),

  CustomPageView(
    getOnBoardPageImage(3),
    CustomColor.onBoardScreen3Background,
    CustomColor.onBoardScreen3Text,
    CustomColor.onBoardScreen3Tab,
    CustomColor.onBoardScreen3NextButton,
    "Secure and\nTrusted",
    "Lorem ipsum Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit",
  ),

  CustomPageView(
    getOnBoardPageImage(4),
    CustomColor.onBoardScreen4Background,
    CustomColor.onBoardScreen4Text,
    CustomColor.onBoardScreen4Tab,
    CustomColor.onBoardScreen4NextButton,
    "Fast and\nReliable",
    "Lorem ipsum Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit",
  ),

  //Sign-in page
  CustomPageView(
    Container(),
    CustomColor.signScreen5Background,
    CustomColor.signScreen5Text,
    CustomColor.signScreen5Tab,
    CustomColor.signScreen5NextButton,
    "Enter your 6 digit PIN to sign in",
    "",
  ),

  //Sign-up page
  CustomPageView(
    Container(),
    CustomColor.signScreen6Background,
    CustomColor.signScreen6Text,
    CustomColor.signScreen6Tab,
    CustomColor.signScreen6NextButton,
    "Enter the following details to sign up",
    "",
  ),

  //Forgot password
  CustomPageView(
    Container(),
    CustomColor.signScreen7Background,
    CustomColor.signScreen7Text,
    CustomColor.signScreen7Tab,
    CustomColor.signScreen7NextButton,
    "Select which contain details should we/nUse to reset your PIN",
    "",
  ),
];

getOnBoardPageImage(int index) {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: (index == 1) ? (140 / 896) * screenHeight : (400 / 896) * screenHeight,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: getPageLogo(index),
              fit: BoxFit.fitHeight,
            ),
          ),
        ),

        (index == 1) ? SizedBox(height: (30 / 896) * screenHeight,) : Container(),

        (index == 1) ? Container(
          height: (32 / 896) * screenHeight,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/1x/onBoardScreen1Text.png'),
              fit: BoxFit.fitHeight,
            ),
          ),
        ) : Container(),
      ],
    ),
  );
}

getPageLogo(int index) {
  switch (index) {
    case 1:
      return AssetImage('assets/1x/onBoardScreen1Logo.png');
    case 2:
      return AssetImage('assets/1x/onBoardScreen2Logo.png');
    case 3:
      return AssetImage('assets/1x/onBoardScreen3Logo.png');
    case 4:
      return AssetImage('assets/1x/onBoardScreen4Logo.png');
  }
}
