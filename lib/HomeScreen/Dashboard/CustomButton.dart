import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final String imagePath;

  CustomButton(this.title, this.imagePath);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: FlatButton(
        padding: EdgeInsets.all(0.0),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: (5 / 448) * screenHeight, vertical: (5 / 207) * screenWidth,),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                width: 0.75 * (72 / 207) * screenWidth,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    this.title,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w500,
                      color: CustomColor.mainSceenText,
                    ),
                  ),
                ),
              ),
              Container(
                height: (20 / 448) * screenHeight,
                width: 0.25 * (72 / 207) * screenWidth,
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Image.asset(this.imagePath, fit: BoxFit.fitHeight,),
                ),
              ),
            ],
          ),
        ),
        onPressed: () {},
      ),
    );
  }
}

List<Widget> listCustomButton = [
  CustomButton("Account\nInformation", 'assets/4x/AccountInformationxxxhdpi.png'),
  CustomButton("Fund\nTransfer", 'assets/4x/FundTransferxxxhdpi.png'),
  CustomButton("Statement", 'assets/4x/Statementxxxhdpi.png'),
  CustomButton("Withdraw", 'assets/4x/Withdrawxxxhdpi.png'),
  CustomButton("Mobile\nPrepaid", 'assets/4x/MobilePrepaidxxxhdpi.png'),
  CustomButton("Bill\nPayment", 'assets/4x/BillPaymentxxxhdpi.png'),
  CustomButton("Credit Card\nService", 'assets/4x/CreditCardServicexxxhdpi.png'),
  CustomButton("Beneficiary", 'assets/4x/Beneficiaryxxxhdpi.png'),
];