import 'package:appbank/CustomColor.dart';
import 'package:appbank/HomeScreen/Dashboard/CustomButton.dart';
import 'package:appbank/HomeScreen/Dashboard/CustomCreditCard.dart';
import 'package:appbank/main.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class DashBoardScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => DashBoardScreenState();
}

class DashBoardScreenState extends State<DashBoardScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[

        SizedBox(height: (10 / 448) * screenHeight,),

        //welcome
        Container(
          height: (10 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
          child: Text(
            "Welcome",
            style: TextStyle(
              fontSize: (10 / 448) * screenHeight,
              fontWeight: FontWeight.w500,
              color: CustomColor.mainSceenText,
            ),
          ),
        ),

        //name
        Container(
          height: (25 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
          child: Text(
            "Alexa!",
            style: TextStyle(
              fontSize: (25 / 448) * screenHeight,
              fontWeight: FontWeight.w600,
              color: CustomColor.mainSceenText,
            ),
          ),
        ),

        SizedBox(height: (10 / 448) * screenHeight,),

        CarouselSlider(
          height: (110 / 448) * screenHeight,
          viewportFraction: 0.83,
          enlargeCenterPage: true,
          items: listCardItem.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return i;
              },
            );
          }).toList(),
        ),

        SizedBox(height: (10 / 448) * screenHeight,),

        Container(
          height: (200 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
          child: GridView.count(
            primary: false,
            mainAxisSpacing: (2 / 448) * screenHeight,
            crossAxisSpacing: (2 / 207) * screenWidth,
            childAspectRatio: ((72 / 207) * screenWidth) / ((35 / 448) * screenHeight),
            crossAxisCount: 2,
            children: <Widget>[
              ...listCustomButton,
            ],
          ),
        ),
      ],
    );
  }
}