import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class CustomCreditCard extends StatelessWidget {

  final double balance;
  final String cardNumber;
  final String name;
  final String expiredDate;
  final Color titleColor;
  final Color color;

  CustomCreditCard(this.balance, this.cardNumber, this.name, this.expiredDate, this.color, this.titleColor);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (110 / 448) * screenHeight,
      width: (163 / 207) * screenWidth,
      decoration: BoxDecoration(
        color: this.color,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      padding: EdgeInsets.symmetric(horizontal: (8 / 448) * screenHeight, vertical: (5 / 207) * screenWidth),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Available balance",
                    style: TextStyle(
                      fontSize: (7 / 448) * screenHeight,
                      fontWeight: FontWeight.w200,
                      color: Colors.white,
                    ),
                  ),

                  SizedBox(height: (3 / 448) * screenHeight,),

                  Text(
                    this.balance.toString(),
                    style: TextStyle(
                      fontSize: (10 / 448) * screenHeight,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),

              Text(
                "MONEY",
                style: TextStyle(
                  fontSize: (20 / 448) * screenHeight,
                  fontWeight: FontWeight.w600,
                  color: this.titleColor,
                ),
              ),
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.name,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),

                  SizedBox(height: (3 / 448) * screenHeight,),

                  Text(
                    this.cardNumber,
                    style: TextStyle(
                      fontSize: (9 / 448) * screenHeight,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),

              Text(
                "Valid till " + this.expiredDate,
                style: TextStyle(
                  fontSize: (6 / 448) * screenHeight,
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

List<Widget> listCardItem = [
  CustomCreditCard(0, "1234 5678 1234 5678", "Alexa Smith", "08/2020", Color.fromRGBO(83, 85, 195, 1.0), Color.fromRGBO(67, 196, 96, 1.0)),
  CustomCreditCard(0, "1245 7896 3456 7984", "Alexa Smith", "08/2020", Color.fromRGBO(42, 55, 71, 1.0), Color.fromRGBO(242, 72, 99, 1.0)),
  CustomCreditCard(0, "5467 9812 5464 6451", "Alexa Smith", "08/2020", Color.fromRGBO(250, 65, 65, 1.0), Color.fromRGBO(255, 255, 255, 1.0)),
];