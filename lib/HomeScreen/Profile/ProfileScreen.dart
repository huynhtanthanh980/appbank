import 'package:appbank/CustomColor.dart';
import 'package:appbank/HomeScreen/Profile/CustomAccountCard.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ProfileScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[

        SizedBox(height: (15 / 448) * screenHeight,),

        //Profile
        Container(
          height: (25 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
          child: Text(
            "Account Info",
            style: TextStyle(
              fontSize: (23 / 448) * screenHeight,
              fontWeight: FontWeight.w300,
              color: CustomColor.mainSceenText,
            ),
          ),
        ),

        SizedBox(height: (15 / 448) * screenHeight,),

        Padding(
          padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
          child: Container(
            height: (60 / 448) * screenHeight,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
              color: Colors.white,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: (55 / 448) * screenHeight,
                  width: (55 / 448) * screenHeight,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: CustomColor.mainSceenBackground,
                  ),
                  child: Icon(
                    Icons.person,
                    size: (45 / 448) * screenHeight,
                  ),
                ),

                Container(
                  height: (55 / 448) * screenHeight,
                  width: (55 / 448) * screenHeight,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Alexa Smith",
                        style: TextStyle(
                          fontSize: (10 / 448) * screenHeight,
                          fontWeight: FontWeight.w500,
                          color: CustomColor.mainSceenText,
                        ),
                      ),

                      SizedBox(height: (5 / 448) * screenHeight,),

                      Text(
                        "Available balance",
                        style: TextStyle(
                          fontSize: (6 / 448) * screenHeight,
                          fontWeight: FontWeight.w500,
                          color: CustomColor.mainSceenText,
                        ),
                      ),
                      Text(
                        "\$2,365.52",
                        style: TextStyle(
                          fontSize: (10 / 448) * screenHeight,
                          fontWeight: FontWeight.w300,
                          color: CustomColor.mainSceenText,
                        ),
                      ),
                    ],
                  ),
                ),

                Container(
                  height: (55 / 448) * screenHeight,
                  width: (55 / 448) * screenHeight,
                  padding: EdgeInsets.all(5.0),
                  alignment: Alignment(1.0, -1.0),
                  child: Container(
                    height: (15 / 448) * screenHeight,
                    width: (40 / 448) * screenHeight,
                    child: RaisedButton(
                      padding: EdgeInsets.all(5.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                      color: CustomColor.mainSceenBackground,
                      elevation: 0.0,
                      child: Text(
                        "Edit profile",
                        style: TextStyle(
                          fontSize: (6 / 448) * screenHeight,
                          fontWeight: FontWeight.w400,
                          color: CustomColor.mainSceenText,
                        ),
                      ),

                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),

        SizedBox(height: (0 / 448) * screenHeight,),

        Container(
          height: (50 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: (15 / 448) * screenHeight, horizontal: (20 / 207) * screenWidth,),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: (25 / 448) * screenHeight,
                width: (80 / 207) * screenWidth,
                child: RaisedButton(
                  padding: EdgeInsets.all(5.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                  color: Color.fromRGBO(67, 196, 98, 1.0),
                  elevation: 0.0,
                  child: Text(
                    "Accounts",
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),

                  onPressed: () {},
                ),
              ),

              Container(
                height: (25 / 448) * screenHeight,
                width: (80 / 207) * screenWidth,
                child: RaisedButton(
                  padding: EdgeInsets.all(5.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(
                      color: Colors.black,
                      width: 1.0,
                    ),
                  ),
                  color: CustomColor.mainSceenBackground,
                  elevation: 0.0,
                  child: Text(
                    "Cards",
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w500,
                      color: CustomColor.mainSceenText,
                    ),
                  ),

                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),

        Expanded(
          child: Container(
            child: ListView.builder(
                itemCount: listAccountCard.length,
                itemBuilder: (context, index) {
                  CustomAccountCard page = listAccountCard[index];
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: (5 / 448) * screenHeight,),
                    child: Slidable(
                      key: Key(page.number),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
                        child: page,
                      ),
                      actionPane: SlidableDrawerActionPane(),
                      actions: <Widget>[
                        IconSlideAction(
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: () {
                            return showDialog<bool>(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Delete'),
                                  content: Text('Item will be deleted'),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text('Cancel'),
                                      onPressed: () => Navigator.of(context).pop(false),
                                    ),
                                    FlatButton(
                                      child: Text('Ok'),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        setState(() {
                                          listAccountCard.removeAt(index);
                                        });
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        ),
                      ],
                      secondaryActions: <Widget>[
                        IconSlideAction(
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: () {
                            return showDialog<bool>(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Delete'),
                                  content: Text('Item will be deleted'),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text('Cancel'),
                                      onPressed: () => Navigator.of(context).pop(false),
                                    ),
                                    FlatButton(
                                      child: Text('Ok'),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        setState(() {
                                          listAccountCard.removeAt(index);
                                        });
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        ),
                      ],
                      dismissal: SlidableDismissal(
                        child: SlidableDrawerDismissal(),
                        onWillDismiss: (actionType) {
                          return showDialog<bool>(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text('Delete'),
                                content: Text('Item will be deleted'),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Cancel'),
                                    onPressed: () => Navigator.of(context).pop(false),
                                  ),
                                  FlatButton(
                                    child: Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                      setState(() {
                                        listAccountCard.removeAt(index);
                                      });
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      ),
                    ),
                  );
                }
            ),
          ),
        ),
      ],
    );
  }
}