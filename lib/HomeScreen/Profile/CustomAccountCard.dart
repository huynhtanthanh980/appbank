import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class CustomAccountCard extends StatelessWidget {

  final String number;
  final String type;
  final String IFSC;

  CustomAccountCard(this.number, this.type, this.IFSC);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (60 / 448) * screenHeight,
      width: (177 / 207) * screenWidth,
      padding: EdgeInsets.symmetric(horizontal: (8 / 207) * screenWidth, vertical: (5 / 207) * screenWidth),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: (70 / 207) * screenWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Account Number",
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),

                Text(
                  "Account Type",
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),

                Text(
                  "IFSC Code",
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),


          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  number,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w600,
                    color: CustomColor.mainSceenText,
                  ),
                ),

                Text(
                  type,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w600,
                    color: CustomColor.mainSceenText,
                  ),
                ),

                Text(
                  IFSC,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w600,
                    color: CustomColor.mainSceenText,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

List<Widget> listAccountCard = [
  CustomAccountCard("1234 5678 1234 5678", "Saving ACcount", "ABCD000612"),
  CustomAccountCard("1234 5678 1234 5679", "Saving ACcount", "ABCD000612"),
  CustomAccountCard("1234 5678 1234 5680", "Saving ACcount", "ABCD000612"),
  CustomAccountCard("1234 5678 1234 5681", "Saving ACcount", "ABCD000612"),
  CustomAccountCard("1234 5678 1234 5682", "Saving ACcount", "ABCD000612"),
  CustomAccountCard("1234 5678 1234 5683", "Saving ACcount", "ABCD000612"),
];