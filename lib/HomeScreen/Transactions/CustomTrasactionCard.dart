import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class CustomTransactionCard extends StatelessWidget {

  final double amount;
  final String purpose;
  final String place;

  CustomTransactionCard(this.amount, this.purpose, this.place);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: (10 / 448) * screenHeight,),
      child: Container(
        height: (50 / 448) * screenHeight,
        width: (177 / 207) * screenWidth,
        padding: EdgeInsets.symmetric(horizontal: (8 / 207) * screenWidth, vertical: (5 / 207) * screenWidth),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: (50 / 448) * screenHeight,
              width: (90 / 207) * screenWidth,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.purpose,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: (10 / 448) * screenHeight,
                      fontWeight: FontWeight.w700,
                      color: CustomColor.mainSceenText,
                    ),
                  ),

                  Text(
                    this.place,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w400,
                      color: CustomColor.mainSceenText,
                    ),
                  ),
                ],
              ),
            ),

            Container(
              height: (50 / 448) * screenHeight,
              width: (70 / 207) * screenWidth,
              alignment: Alignment(1.0, 0.0),
              child: Text(
                "\$" + this.amount.toString(),
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: (10 / 448) * screenHeight,
                  fontWeight: FontWeight.w600,
                  color: (this.amount > 0) ? Colors.green : Colors.red,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<Widget> listTransactionCard = [
  CustomTransactionCard(-123.0, "Shopping", "PV"),
  CustomTransactionCard(123.0, "Shopping", "PV"),
  CustomTransactionCard(-123.0, "Shopping", "PV"),
  CustomTransactionCard(123.0, "Shopping", "PV"),
  CustomTransactionCard(-123.0, "Shopping", "PV"),
  CustomTransactionCard(123.0, "Shopping", "PV"),
];