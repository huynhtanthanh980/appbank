import 'dart:async';

import 'package:appbank/CustomColor.dart';
import 'package:appbank/HomeScreen/Transactions/CustomTrasactionCard.dart';
import 'package:appbank/main.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class TransactionScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => TransactionScreenState();
}

class TransactionScreenState extends State<TransactionScreen> {

  List<BarChartGroupData> rawBarGroups;
  List<BarChartGroupData> showingBarGroups;

  StreamController<BarTouchResponse> barTouchedResultStreamController;

  int touchedGroupIndex;

  double highValue = 80.0;
  double lowValue = 20.0;

  @override
  void initState() {
    super.initState();

    final items = [
      makeGroupData(0, 12),
      makeGroupData(1, 55),
      makeGroupData(2, 62),
      makeGroupData(3, 100),
      makeGroupData(4, 21),
      makeGroupData(5, 35),
      makeGroupData(6, 15),
    ];

    rawBarGroups = items;

    showingBarGroups = rawBarGroups;

    barTouchedResultStreamController = StreamController();
    barTouchedResultStreamController.stream.distinct().listen((BarTouchResponse response) {
      if (response == null) {
        return;
      }

      if (response.spot == null) {
        setState(() {
          touchedGroupIndex = -1;
          showingBarGroups = List.of(rawBarGroups);
        });
        return;
      }

      touchedGroupIndex = showingBarGroups.indexOf(response.spot.touchedBarGroup);

      setState(() {
        if (response.touchInput is FlLongPressEnd) {
          touchedGroupIndex = -1;
          showingBarGroups = List.of(rawBarGroups);
        } else {
          showingBarGroups = List.of(rawBarGroups);
          if (touchedGroupIndex != -1) {
            showingBarGroups[touchedGroupIndex] = showingBarGroups[touchedGroupIndex].copyWith(
              barRods: showingBarGroups[touchedGroupIndex].barRods.map((rod) {
                return rod.copyWith(y: rod.y);
              }).toList(),
            );
          }
        }
      });
    });
  }

  BarChartGroupData makeGroupData(int x, double y) {
    return BarChartGroupData(x: x, barRods: [
      BarChartRodData(
        y: y,
        color: (y > highValue) ? Color.fromRGBO(249, 53, 67, 1.0) : (y < lowValue) ? Color.fromRGBO(0, 173, 152, 1.0) : Color.fromRGBO(255, 187, 0, 1.0),
        width: 10,
        isRound: true,
        //Background of the bar
        backDrawRodData: BackgroundBarChartRodData(
          show: false,
          //MaxHeight
          y: 110,
        ),
      ),
    ]);
  }

  @override
  void dispose() {
    super.dispose();
    barTouchedResultStreamController.close();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[

        SizedBox(height: (15 / 448) * screenHeight,),

        //Transactions
        Container(
          height: (25 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
          child: Text(
            "Transactions",
            style: TextStyle(
              fontSize: (23 / 448) * screenHeight,
              fontWeight: FontWeight.w300,
              color: CustomColor.mainSceenText,
            ),
          ),
        ),

        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: (14 / 207) * screenWidth,),
            child: ListView.builder(
              itemCount: listTransactionCard.length + 1,
              itemBuilder: (context, index) {
                return (index == 0) ? getChart() : listTransactionCard[index - 1];
              },
            ),
          ),
        ),
      ],
    );
  }

  getChart() {
    return Column(
      children: <Widget>[
        SizedBox(height: (10 / 448) * screenHeight,),

        Container(
          height: (175 / 448) * screenHeight,
          width: (170 / 207) * screenWidth,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: Colors.white,
          ),
          child: Column(
            children: <Widget>[
              Container(
                height: (25 / 448) * screenHeight,
                width: double.infinity,
                padding: EdgeInsets.all(10.0),
                alignment: Alignment(1.0, 0.0),
                child: Container(
                  width: (45 / 207) * screenWidth,
                  child: RaisedButton(
                    padding: EdgeInsets.all(4.0),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                    color: Color.fromRGBO(236, 236, 237, 1.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          "This week",
                          style: TextStyle(
                            fontSize: (6 / 448) * screenHeight,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Icon(Icons.keyboard_arrow_down),
                      ],
                    ),

                    onPressed: () {},
                  ),
                ),
              ),

              Row(
                children: <Widget>[
                  Container(
                    height: (142 / 448) * screenHeight,
                    width: (15 / 207) * screenWidth,
                    alignment: Alignment(0.0, 1.0),
                    child: IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {},
                    ),
                  ),

                  Container(
                    height: (142 / 448) * screenHeight,
                    width: (140 / 207) * screenWidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 20.0),
                            child: FlChart(
                              chart: BarChart(BarChartData(
                                barTouchData: BarTouchData(
                                  touchTooltipData: TouchTooltipData(
                                      tooltipBgColor: Color.fromRGBO(42, 55, 71, 1.0),
                                      getTooltipItems: (touchedSpots) {
                                        return touchedSpots.map((touchedSpot) {
                                          return TooltipItem("\$" + touchedSpot.spot.y.toString(), TextStyle(color: Colors.white));
                                        }).toList();
                                      }
                                  ),
                                  touchResponseSink: barTouchedResultStreamController.sink,
                                ),
                                titlesData: FlTitlesData(
                                  show: false,
                                ),
                                borderData: FlBorderData(
                                  show: false,
                                ),
                                barGroups: showingBarGroups,
                              )),
                            ),
                          ),
                        ),

                        Container(
                          height: (22 / 448) * screenHeight,
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              ...getChartBottomButton(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    height: (142 / 448) * screenHeight,
                    width: (15 / 207) * screenWidth,
                    alignment: Alignment(0.0, 1.0),
                    child: IconButton(
                      icon: Icon(Icons.arrow_forward_ios),
                      onPressed: () {},
                    ),
                  ),

                ],
              ),
            ],
          ),
        ),

        SizedBox(height: (10 / 448) * screenHeight,),

        Container(
          height: (25 / 448) * screenHeight,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Recent Transactions",
                style: TextStyle(
                  fontSize: (10 / 448) * screenHeight,
                  fontWeight: FontWeight.w400,
                  color: CustomColor.mainSceenText,
                ),
              ),

              RaisedButton(
                padding: EdgeInsets.all(5.0),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                color: CustomColor.mainSceenBackground,
                elevation: 0.0,
                child: Row(
                  children: <Widget>[
                    Text(
                      "View all",
                      style: TextStyle(
                        fontSize: (8 / 448) * screenHeight,
                        fontWeight: FontWeight.w400,
                        color: CustomColor.mainSceenText,
                      ),
                    ),
                    Icon(
                      Icons.keyboard_arrow_right,
                      size: (10 / 448) * screenHeight,
                    ),
                  ],
                ),

                onPressed: () {},
              ),
            ],
          ),
        ),
      ],
    );
  }

  getChartBottomButton() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < 7; i++) {
      String title;

      switch(i) {
        case 0:
          title = "S";
          break;
        case 1:
          title = "M";
          break;
        case 2:
          title = "T";
          break;
        case 3:
          title = "W";
          break;
        case 4:
          title = "T";
          break;
        case 5:
          title = "F";
          break;
        case 6:
          title = "S";
          break;
      }

      list.add( new Container(
        height: (14 / 448) * screenHeight,
        width: (14 / 207) * screenWidth,
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(30.0),
          color: Color.fromRGBO(242, 242, 243, 1.0),
        ),
        child:Center(
          child: Text(
            title,
            style: TextStyle(
              fontSize: (6 / 448) * screenHeight,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),);
    }

    return list;
  }
}