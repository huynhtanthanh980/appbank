import 'package:appbank/CustomColor.dart';
import 'package:appbank/HomeScreen/Dashboard/DashBoardScreen.dart';
import 'package:appbank/HomeScreen/Nofications/NotificationScreen.dart';
import 'package:appbank/HomeScreen/Profile/ProfileScreen.dart';
import 'package:appbank/HomeScreen/Transactions/TransactionScreen.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';

class MainScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {

  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();

  int currentPage = 1;

  bool isDrawerOpened = false;

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return InnerDrawer(
      key: _innerDrawerKey,
      onTapClose: true,
      leftOffset: 0.5,
      leftChild: getLeftDrawer(),
      innerDrawerCallback: (isOpened) {
        setState(() {
          isDrawerOpened = isOpened;
        });
      },
      scaffold: Scaffold(
        backgroundColor: CustomColor.mainSceenBackground,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: CustomColor.mainSceenBackground,
          automaticallyImplyLeading: false,
          title: (isDrawerOpened) ? null : getAppBar(),
        ),

        body: getBody(),

        bottomNavigationBar: getBottomBar(),
      ),
    );
  }

  getLeftDrawer() {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          getAppBar(),

          SizedBox(height: (20 / 448) * screenHeight,),

          getDrawerButton(1, "Services", Icons.build),

          getBlackLine(),

          getDrawerButton(2, "Benefits", Icons.videogame_asset),

          getBlackLine(),

          getDrawerButton(3, "Scan To Pay", Icons.camera),

          getBlackLine(),

          getDrawerButton(4, "Reward", Icons.card_giftcard),

          getBlackLine(),

          getDrawerButton(5, "App Settings", Icons.settings),

          getBlackLine(),

          getDrawerButton(6, "FAQs", Icons.info),

          getBlackLine(),

          getDrawerButton(7, "Support", Icons.question_answer),

          getBlackLine(),

          getDrawerButton(8, "Feedback", Icons.feedback),
        ],
      ),
    );
  }

  getBlackLine() {
    return Container(
      height: (0.2 / 448) * screenHeight,
      width: 0.68 * screenWidth,
      color: Colors.black26,
    );
  }

  getDrawerButton(int index, String tabName, IconData icon) {
    return Container(
      height: (40 / 448) * screenHeight,
      width: double.infinity,
      child: FlatButton(
        padding: EdgeInsets.all(0.0),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: (10 / 207) * screenWidth,),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    icon,
                    size: (15 / 448) * screenHeight,
                  ),

                  SizedBox(width: (10 / 207) * screenWidth,),

                  Text(
                    tabName,
                    style: TextStyle(
                      fontSize: (8 / 448) * screenHeight,
                      fontWeight: FontWeight.w600,
                      color: CustomColor.mainSceenText,
                    ),
                  ),
                ],
              ),

              Icon(
                Icons.keyboard_arrow_right,
                size: (15 / 448) * screenHeight,
              ),
            ],
          ),
        ),
        onPressed: () {},
      )
    );
  }

  getAppBar() {
    return Container(
      padding: EdgeInsets.only(top: (12 / 448) * screenHeight,),
      child: Container(
          height: (15 / 448) * screenHeight,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: double.infinity,
                width: (30 / 207) * screenWidth,
                child: FlatButton(
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    child: Image.asset(
                      (isDrawerOpened) ? 'assets/4x/MenuFlippedxxxhdpi.png' : 'assets/4x/Menuxxxhdpi.png',
                      fit: BoxFit.fitHeight,

                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      isDrawerOpened = true;
                    });
                    _innerDrawerKey.currentState.toggle(
                        direction: InnerDrawerDirection.start,
                    );
                  },
                ),
              ),

              Container(
                height: double.infinity,
                width: (30 / 207) * screenWidth,
                child: FlatButton(
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    child: Image.asset(
                      'assets/4x/Powerxxxhdpi.png',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
    );
  }

  getBottomBar() {
    return Container(
      height: (40 / 448) * screenHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0), topRight: Radius.circular(50.0),),
        color: Colors.white,
      ),
      child:Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: (0 / 207) * screenWidth,),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            getBottomBarButton(1, "Dashboard", Icons.dashboard),
            getBottomBarButton(2, "Transactions", Icons.assessment),
            getBottomBarButton(3, "Notifications", Icons.notifications),
            getBottomBarButton(4, "Profile", Icons.person),
          ],
        ),
      ),
    );
  }

  getBottomBarButton(int index, String tab, IconData icon) {
    return Container(
      width: (50 / 207) * screenWidth,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
      ),
      child: FlatButton(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                size: (15 / 448) * screenHeight,
                color: (currentPage == index) ? CustomColor.mainSceenText : Color.fromRGBO(153, 153, 153, 1.0),
              ),
              Text(
                tab,
                style: TextStyle(
                  fontSize: (5.5 / 448) * screenHeight,
                  fontWeight: FontWeight.w600,
                  color: (currentPage == index) ? CustomColor.mainSceenText : Color.fromRGBO(153, 153, 153, 1.0),
                ),
              ),
            ],
          ),
        ),
        onPressed: () {
          setState(() {
            currentPage = index;
          });
        },
      ),
    );
  }

  getBody() {
    switch (currentPage) {
      case 1:
        return DashBoardScreen();
      case 2:
        return TransactionScreen();
      case 3:
        return NotificationScreen();
      case 4:
        return ProfileScreen();
    }
  }
}

