import 'package:appbank/CustomColor.dart';
import 'package:appbank/HomeScreen/Nofications/CustomNoficationCard.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class NotificationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => NotificationScreenState();
}

class NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: SizedBox(
                height: (15 / 448) * screenHeight,
              ),
            ),
          ),
        ),

        //Nofication
        Container(
          height: (25 / 448) * screenHeight,
          width: double.infinity,
          padding: EdgeInsets.symmetric(
            horizontal: (14 / 207) * screenWidth,
          ),
          child: Text(
            "Nofications",
            style: TextStyle(
              fontSize: (23 / 448) * screenHeight,
              fontWeight: FontWeight.w300,
              color: CustomColor.mainSceenText,
            ),
          ),
        ),

        SizedBox(
          height: (10 / 448) * screenHeight,
        ),

        Expanded(
          child: Container(
            child: ListView.builder(
                itemCount: listNotificationCard.length,
                itemBuilder: (context, index) {
                  CustomNotificationCard page = listNotificationCard[index];
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: (5 / 448) * screenHeight,
                    ),
                    child: Slidable(
                      key: Key(UniqueKey().toString()),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: (14 / 207) * screenWidth,
                        ),
                        child: page,
                      ),
                      actionPane: SlidableDrawerActionPane(),
                      actions: <Widget>[
                        IconSlideAction(
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: () {
                            return showDialog<bool>(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Repeat'),
                                  content:
                                      Text('Item will be notified in 1 hour'),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text('Cancel'),
                                      onPressed: () =>
                                          Navigator.of(context).pop(false),
                                    ),
                                    FlatButton(
                                      child: Text('Ok'),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        setState(() {
                                          listNotificationCard.removeAt(index);
                                        });
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        ),
                      ],
                      secondaryActions: <Widget>[
                        IconSlideAction(
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: () {
                            return showDialog<bool>(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Repeat'),
                                  content:
                                      Text('Item will be notified in 1 hour'),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text('Cancel'),
                                      onPressed: () =>
                                          Navigator.of(context).pop(false),
                                    ),
                                    FlatButton(
                                      child: Text('Ok'),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        setState(() {
                                          listNotificationCard.removeAt(index);
                                        });
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        ),
                      ],
                      dismissal: SlidableDismissal(
                        child: SlidableDrawerDismissal(),
                        onWillDismiss: (actionType) {
                          return showDialog<bool>(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text('Delete'),
                                content: Text('Item will be deleted'),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Cancel'),
                                    onPressed: () =>
                                        Navigator.of(context).pop(false),
                                  ),
                                  FlatButton(
                                    child: Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                      setState(() {
                                        listNotificationCard.removeAt(index);
                                      });
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      ),
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}
