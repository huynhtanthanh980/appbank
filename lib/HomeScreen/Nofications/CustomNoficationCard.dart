import 'package:appbank/CustomColor.dart';
import 'package:appbank/main.dart';
import 'package:flutter/material.dart';

class CustomNotificationCard extends StatelessWidget {

  final String title;
  final String content;
  final DateTime time;

  CustomNotificationCard(this.title, this.content, this.time);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (50 / 448) * screenHeight,
      width: (177 / 207) * screenWidth,
      padding: EdgeInsets.symmetric(horizontal: (8 / 207) * screenWidth, vertical: (5 / 207) * screenWidth),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular((5.0))),
        color: Colors.white,
      ),
      child: Row(
        children: <Widget>[
          Container(
            height: (30 / 207) * screenWidth,
            width: (30 / 207) * screenWidth,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.grey,
            ),
            child: Icon(
              Icons.person,
              size: (30 / 207) * screenWidth,
              color: Colors.white,
            ),
          ),

          SizedBox(width: (10 / 207) * screenWidth,),

          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: (80 / 207) * screenWidth,
                      child: Text(
                        title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: (8 / 448) * screenHeight,
                          fontWeight: FontWeight.w600,
                          color: CustomColor.mainSceenText,
                        ),
                      ),
                    ),

                    Container(
                      width: (40 / 207) * screenWidth,
                      alignment: Alignment(1.0, 0.0),
                      child: Text(
                        (checkDate(time)) ? (time.hour.toString() + ":" + time.minute.toString()) : (time.day.toString() + "/" + time.month.toString() + "/" + time.year.toString()),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: (8 / 448) * screenHeight,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),

                Text(
                  content,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: (8 / 448) * screenHeight,
                    fontWeight: FontWeight.w400,
                    color: CustomColor.mainSceenText,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

checkDate(DateTime time) {
  DateTime now = new DateTime.now();
  return (time.day.toString() == now.day.toString()) & (time.month.toString() == now.month.toString()) & (time.year.toString() == now.year.toString());
}

List<Widget> listNotificationCard = [
  CustomNotificationCard("TestTitlsafddddddddddddddddddddddddddde", "Longgggggggggggggggggggggggg Test Content", new DateTime(10, 10, 10000000)),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime(10, 10, 2019)),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
  CustomNotificationCard("Test Title", "Longgggggggggggggggggggggggg Test Content", new DateTime.now()),
];