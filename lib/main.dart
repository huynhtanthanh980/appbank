import 'package:appbank/HomeScreen/MainScreen.dart';
import 'package:appbank/OnBoard/OnBoardScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

var screenHeight;
var screenWidth;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setEnabledSystemUIOverlays([]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: OnBoardScreen(),
      routes: <String, WidgetBuilder>{
        '/MainScreen': (BuildContext context) => new MainScreen(),
      },
    );
  }
}
